import java.util.List;
import java.util.PriorityQueue;

class SupermarketQueue {

    int queueTime(List<Integer> clientsTime, int tiles) {

        int time = 0;
        PriorityQueue<Integer> tileTimes = new PriorityQueue<>();
        for(int i = 0; i < tiles; ++i) tileTimes.add(0);

        for(Integer clientTime: clientsTime){
            int top = tileTimes.poll();
            if(top > 0){
                PriorityQueue<Integer> tileTimesAux = new PriorityQueue<>();
                for(int tileTime: tileTimes){
                    tileTimesAux.add(tileTime-top);
                }
                tileTimes = tileTimesAux;
                time +=top;
            }
            tileTimes.add(clientTime);
        }

        int max = 0;
        for(int tileTime: tileTimes){
            if(tileTime > max){
                max = tileTime;
            }
        }
        time += max;

        return time;
    }
}
