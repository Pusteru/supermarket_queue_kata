import java.util.List;
import java.util.PriorityQueue;

class SupermarketQueueV2 {

    int queueTime(List<Integer> customers, int n) {
        int time = 0;
        Tiles tiles = new Tiles(n);
        for (Integer clientTime : customers) {
            int top = tiles.getMinTime();
            if(top > 0){
                tiles.decreaseTime(top);
                time +=top;
            }
            tiles.add(clientTime);
        }
        time += tiles.getMax();
        return time;
    }

}


class Tiles {

    private PriorityQueue<Integer> tileTimes = new PriorityQueue<>();

    Tiles(int tiles){
        for (int i = 0; i < tiles; ++i) this.tileTimes.add(0);
    }

    int getMinTime(){
        return this.tileTimes.poll();
    }

    void add(Integer clientTime) {
        this.tileTimes.add(clientTime);
    }

    int getMax(){
        int max = 0;
        for (int tileTime : this.tileTimes) {
            if(tileTime > max){
                max = tileTime;
            }
        }
        return max;
    }

    void decreaseTime(int top) {
        PriorityQueue<Integer> tileTimesAux = new PriorityQueue<>();
        for (int tileTime : this.tileTimes) {
            tileTimesAux.add(tileTime-top);
        }
        this.tileTimes = tileTimesAux;
    }
}
