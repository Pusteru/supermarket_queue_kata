import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class SupermarketQueueLambdas {


    private boolean notFound;

    int queueTime(List<Integer> clientsTime, int tiles) {

        List<TileL> tileTime = Stream.generate(TileL::new).limit(tiles).collect(Collectors.toList());
        int time = 0;
        for (Integer clientTime : clientsTime) {
            int top = tileTime.stream().min(TileL::compare).get().getTime();
            if (top > 0) {
                tileTime = tileTime.stream().map(x -> x.decrease(top)).collect(Collectors.toList());
            }
            this.notFound = true;
            tileTime = tileTime.stream().map(x -> {
                if (x.isFree() && this.notFound) {
                    x.setTime(clientTime);
                    this.notFound = false;
                }
                return x;
            }).collect(Collectors.toList());
            time += top;
        }
        time += tileTime.stream().max(TileL::compare).get().getTime();
        return time;
    }


}


class TileL {

    private int time;

    TileL() {
        this.time = 0;
    }

    TileL decrease(int amount) {
        this.time -= amount;
        return this;
    }

    int getTime() {
        return this.time;
    }

    static int compare(TileL tileL, TileL tileL1) {
        return Integer.compare(tileL.getTime(), tileL1.getTime());
    }

    boolean isFree() {
        return this.time == 0;
    }

    void setTime(int time) {
        this.time = time;
    }
}
