import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

class SupermarketQueueV3 {

    int queueTime(List<Integer> customers, int n) {
        final PriorityQueue<Integer> queue = new PriorityQueue<>(Collections.nCopies(n, 0));
        for (int customer : customers) queue.add(queue.poll() + customer);
        return Collections.max(queue);
    }

}


