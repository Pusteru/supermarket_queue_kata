import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class QueueTimeTest {

    private List<Integer> clientsTime = new ArrayList<>();
    private int tiles;
    private int total;

    @Before
    public void init(){
        this.clientsTime.add(3);
        this.clientsTime.add(10);
        this.clientsTime.add(7);
        this.clientsTime.add(1);
        this.clientsTime.add(8);
        this.tiles = 2;
        this.total = 18;
    }

    @Test
    public void correctQueueTime(){
        SupermarketQueue supermarketQueue = new SupermarketQueue();
        assertThat(supermarketQueue.queueTime(this.clientsTime, this.tiles)).isEqualTo(this.total);

    }

    @Test
    public void correctQueueTimeV2(){
        SupermarketQueueV2 supermarketQueueV2 = new SupermarketQueueV2();
        assertThat(supermarketQueueV2.queueTime(this.clientsTime, this.tiles)).isEqualTo(this.total);
    }


    @Test
    public void correctQueueTimeV3(){
        SupermarketQueueV3 supermarketQueueV3 = new SupermarketQueueV3();
        assertThat(supermarketQueueV3.queueTime(this.clientsTime, this.tiles)).isEqualTo(this.total);
    }

    @Test
    public void correctQueueTimeLambdas() {
        SupermarketQueueLambdas supermarketQueueLambdas = new SupermarketQueueLambdas();
        assertThat(supermarketQueueLambdas.queueTime(this.clientsTime, this.tiles)).isEqualTo(this.total);
    }

}
